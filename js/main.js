
var handleEvents = function() {
	$('.js-close-info').on('click', function(){
		$('.info, .js-info-nav').addClass('is-empty');
		$('.chapter__content, .js-story-nav').removeClass('is-empty');
		$('.info__outer').html('');
	});
	$('.js-moreinfo').on('click', function(){
		var chapter = $(this).parent().data('chapter');
		loadInfo(chapter);
	});
	$('.js-lang-nav').on('click', function(){
		if(!$('.lang-selector').hasClass('is-open')){
			$('.lang-selector__option').slideDown();
			$('.lang-selector').addClass('is-open');
		}else{
			//$('.lang-selector__option').slideDown();
			$('.lang-selector__option:not(.lang-selector__option--is-active)').slideUp();
			$('.lang-selector').removeClass('is-open');
		}
	});
	// Cookies
	$('.js-cookies-popup').on('click', function(e) {
		e.preventDefault();
		window.loadPopup();
	});
}

var preloader = function(){
	$('.loader').addClass('is-loading');
	setTimeout(function () { 
		$('.loader').addClass('is-loaded');
	}, 800);
}

var getURI = function(){
	var currentURL = window.location.href;
	var lastURLSegment = currentURL.substr(currentURL.lastIndexOf('/') + 1);
	var pages = ['agua-dulce', 'espacios-naturales', 'rios', '21-marzo', 'pura-naturaleza'];
	var initPosition = pages.indexOf(lastURLSegment) >= 0 ? pages.indexOf(lastURLSegment) : 0;
	storySlider(initPosition);
	stepsPaginationStory(initPosition);
	hashNavigate(initPosition);
}

var storySlider = function(initPosition){
	var story = new Swiper('.story-container', {
		containerModifierClass: 'story-container-',
		wrapperClass: 'story-wrapper',
		slideClass: 'story-slide',
		slideActiveClass: 'story-slide-active',
		slideNextClass: 'story-slide-next',
		slidePrevClass: 'story-slide-prev',
	    speed: 400,
	    initialSlide: initPosition,
	    watchSlidesProgress: true,
	    navigation: {
	    	nextEl: '.story-button-next',
    		prevEl: '.story-button-prev',
	    },
	    a11y: {
	    	//Accessibility Parameters
	    	//TO DO:
	    	//Messages for screen readers when actions
	    	prevSlideMessage: 'Anterior slide',
    		nextSlideMessage: 'Siguiente slide',
	    	notificationClass: 'story-notification'
	    },
	    /*hashNavigation: {
			replaceState: true,
			watchState: true
		},*/
		/*history: {
			replaceState: true,
			key: 'sostenibilidad'
		}*/
		on: {
			slideChange: function(){
				stepsPaginationStory(this.activeIndex);
				hashNavigate(this.activeIndex);
			}
		}
	});
}

//Steps Pagination Stories
var stepsPaginationStory = function( activeSlide ){
	$('.story-pagination__chapter').each(function( index ) {
		var $step = $(this);
		var chapterNumber = $step.data('chapter');
		if( chapterNumber <= activeSlide ){
			$step.addClass('is-completed');
		}else{
			$step.removeClass('is-completed');
		}
	});
}

var loadInfo = function(chapter){
	var url = "content/info-" + chapter + ".php";
	//var url = "http://fontvellaespre.housings.nexica.net/content/info-" + chapter + ".php";
	$( '.info__outer' ).load( url, function() {
		//changeContent
		$('.chapter__content, .js-story-nav').addClass('is-empty');
		TweenMax.set(".info__panel img, .info__panel h1, .info__panel p, .panel-image", {y:20,opacity:0});
		$('.info, .js-info-nav').removeClass('is-empty');
		setTimeout(function() {
		  TweenMax.staggerTo(".info__panel img, .info__panel h1, .info__panel p, .panel-image", 1, {y:0,opacity:1}, 0.2);
		}, 300);
	});
}

var hashNavigate = function(activeIndex) {
	var hash = $('[data-chapter="' + activeIndex + '"]').data('hash');
	window.history.pushState({url: hash}, '', hash);
}


$(document).ready(function(){
	console.log('Say Hey JS!!');
	preloader();
	getURI();
	handleEvents();
});