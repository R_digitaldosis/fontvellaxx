<?php require_once('../translate.php');?>
<header class="info__header">
    <div class="info__panel">
        <img src="img/icons/ico-slide-03.svg" alt="Icono Beneficios Agua Font Vella"/>
        <h1 class="title"><?php echo t('SLIDE_4_INFO_title') ?></h1>
        <p class="subtitle"><?php echo t('SLIDE_4_INFO_subtitle') ?></p>
    </div>
    <img class="panel-image" src="img/info/img-sabermas-02.jpg" alt="">
</header>
<section class="info__content">
    <p><?php echo t('SLIDE_4_INFO_CONTENT_text_1') ?></p>
    
    <p><?php echo t('SLIDE_4_INFO_CONTENT_text_2') ?></p>

    <p><?php echo t('SLIDE_4_INFO_CONTENT_text_3') ?></p>

    <p class="last"><?php echo t('SLIDE_4_INFO_CONTENT_text_4') ?></p>

    <div class="info__btn">
        <div class="inner-btn"><a href=""><?php echo t('SLIDE_4_INFO_CONTENT_btn') ?></a></div>
    </div>
</section>