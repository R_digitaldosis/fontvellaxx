<?php require_once('../translate.php');?>
<header class="info__header">
    <div class="info__panel">
        <img src="img/icons/ico-slide-04.svg" alt="Icono Espacio Natural Agua Font Vella"/>
        <h1 class="title"><?php echo t('SLIDE_3_INFO_title') ?></h1>
        <p class="subtitle"><?php echo t('SLIDE_3_INFO_subtitle') ?></p>
    </div>
    <img class="panel-image" src="img/info/img-sabermas-03.jpg" alt="">
</header>
<section class="info__content">
    <img src="img/info/logo-anse.jpg" alt="Logotipo ANSE" class="logo-anse">
    <h2 class="outer-title"><?php echo t('SLIDE_3_INFO_CONTENT_title_1') ?></h2>
    <p><?php echo t('SLIDE_3_INFO_CONTENT_text_1') ?></p>
    <div class="info__list">
        <h4><?php echo t('SLIDE_3_INFO_CONTENT_title_list') ?></h4>
        <p><?php echo t('SLIDE_3_INFO_CONTENT_list_1') ?></p>
        <p><?php echo t('SLIDE_3_INFO_CONTENT_list_2') ?></p>
        <p><?php echo t('SLIDE_3_INFO_CONTENT_list_3') ?></p>
        <p><?php echo t('SLIDE_3_INFO_CONTENT_list_4') ?></p>
    </div>
    <p class="last"><?php echo t('SLIDE_3_INFO_CONTENT_text_2') ?></p>
    <img src="img/info/logo-red-cambera.jpg" alt="Logotipo Red Cambera" class="logo-red-cambera">
    <h2><?php echo t('SLIDE_3_INFO_CONTENT_title_2') ?></h2>
    <p class="last"><?php echo t('SLIDE_3_INFO_CONTENT_text_3') ?></p>
    <div class="info__list">
        <h4><?php echo t('SLIDE_3_INFO_CONTENT_title_list-2') ?></h4>
        <p><?php echo t('SLIDE_3_INFO_CONTENT_list-2_1') ?></p>
        <p><?php echo t('SLIDE_3_INFO_CONTENT_list-2_2') ?></p>
        <p><?php echo t('SLIDE_3_INFO_CONTENT_list-2_3') ?></p>
        <p><?php echo t('SLIDE_3_INFO_CONTENT_list-2_4') ?></p>
    </div>
    <p class="last"><?php echo t('SLIDE_3_INFO_CONTENT_text_4') ?></p>

    <p class="text-center"><?php echo t('SLIDE_3_INFO_CONTENT_img_credits') ?></p>
    <div class="info__logos">
        <a href="https://fundacion-biodiversidad.es/es"><img src="img/info/logo-fundacion.jpg" alt="Logotipo Ministerio para la transición ecológica" class="logo-ministerio"></a>
    </div>
    <div class="info__btn">
        <div class="inner-btn"><a href=""><?php echo t('SLIDE_3_INFO_CONTENT_btn') ?></a></div>
    </div>
</section>