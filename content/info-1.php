<?php require_once('../translate.php');?>
<header class="info__header">
    <div class="info__panel">
        <img src="img/icons/ico-slide-02.svg" alt="Icono Preservar Agua Font Vella"/>
        <h1 class="title"><?php echo t('SLIDE_2_INFO_title') ?></h1>
        <p class="subtitle"><?php echo t('SLIDE_2_INFO_subtitle') ?></p>
    </div>
    <img class="panel-image" src="img/info/img-sabermas-04.jpg" alt="">
</header>
<section class="info__content">
    <p class="last"><?php echo t('SLIDE_2_INFO_CONTENT_text_1') ?></p>
    
    <img src="img/info/img-02-01.jpg" alt="Convenio Selvans con Font Vella y Ayuntamiento Sant Hilari" class="info__image info__image--medium">
    
    <h3><?php echo t('SLIDE_2_INFO_CONTENT_title_1') ?></h3>

    <p class="last"><?php echo t('SLIDE_2_INFO_CONTENT_text_2') ?></p>

    <h3 class="outer-title"><?php echo t('SLIDE_2_INFO_CONTENT_title_2') ?></h3>

    <p><?php echo t('SLIDE_2_INFO_CONTENT_text_3') ?></p>

    <img src="img/info/img-02-02.jpg" alt="Convenio Micorriza" class="info__image info__image--medium">

    <h3 class="outer-title"><?php echo t('SLIDE_2_INFO_CONTENT_title_3') ?></h3>

    <p class="last"><?php echo t('SLIDE_2_INFO_CONTENT_text_4') ?></p>

    <div class="info__btn">
        <div class="inner-btn"><a href=""><?php echo t('SLIDE_2_INFO_CONTENT_btn') ?></a></div>
    </div>
</section>