<?php require_once('../translate.php');?>
<header class="info__header">
    <div class="info__panel">
        <img src="img/icons/ico-slide-01.svg" alt="Icono Gota de Agua Font Vella"/>
        <h1 class="title title--sm"><?php echo t('SLIDE_1_INFO_title') ?></h1>
        <p class="subtitle"><?php echo t('SLIDE_1_INFO_subtitle') ?></p>
    </div>
    <img class="panel-image" src="img/info/img-sabermas-01.jpg" alt="">
</header>
<section class="info__content">
    <p><?php echo t('SLIDE_1_INFO_CONTENT_text_1') ?></p>
    <img src="<?php echo t('SLIDE_1_INFO_CONTENT_img-src_1') ?>" alt="Gráfico agua dulce accesible en el planeta" class="world-graphic is-mobile">
    <img src="<?php echo t('SLIDE_1_INFO_CONTENT_img-src_2') ?>" alt="Gráfico agua dulce accesible en el planeta" class="world-graphic is-desktop">
    <p class="last"><?php echo t('SLIDE_1_INFO_CONTENT_text_2') ?></p>

    <h2><?php echo t('SLIDE_1_INFO_CONTENT_title_1') ?></h2>

    <p><?php echo t('SLIDE_1_INFO_CONTENT_text_3') ?></p>

    <p><?php echo t('SLIDE_1_INFO_CONTENT_text_4') ?></p>

    <p><?php echo t('SLIDE_1_INFO_CONTENT_text_5') ?></p>

    <p class="last"><?php echo t('SLIDE_1_INFO_CONTENT_text_6') ?></p>

    <div class="info__btn">
        <div class="inner-btn"><a href=""><?php echo t('SLIDE_1_INFO_CONTENT_btn') ?></a></div>
    </div>
</section>