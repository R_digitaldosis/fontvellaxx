<!DOCTYPE html>
<html lang="<?php echo getLanguage();?>">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Font Vella</title>
  <link rel="stylesheet" href="<?php echo site('root').site('subdomain').'css/main.min.css'?>">
  <link rel="stylesheet" href="<?php echo site('root').site('subdomain').'css/vendors.min.css'?>">
  <script type="text/javascript" src="https://connectdanoneesprew4.housings.nexica.net/WATS-FONTV201804/privacy_es.js" ></script>
  <script src="<?php echo site('root').site('subdomain').'js/vendors.min.js'?>" charset="utf-8"></script>
  <!-- Google Tag Manager -->
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
    new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
    j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
    'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-TBXMQF2');</script>
    <!-- End Google Tag Manager -->
</head>
<body>
    <!-- Google Tag Manager (noscript) -->
    <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-TBXMQF2"
    height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
    <!-- End Google Tag Manager (noscript) -->
    <div class="page story">
        <div class="loader js-loader">
          <div class="loader__box js-loader-box">
            <div class="loader__background"></div>
            <div class="loader__progress js-progress"></div>
            <span class="loader__logo">
              <img src="<?php echo site('root').site('subdomain').'img/icons/logo-preloader.png'?>"/>
            </span>
          </div>
        </div>
        <header class="header header--story">
            <div class="header__logo"><a href="/"><h1>Font Vella</h1></a></div>
            <div class="header__lang-selector js-story-nav">
                <div class="lang-selector">
                    <span class="lang-selector__arrow js-lang-nav"></span>
                    <a class="lang-selector__option <?php echo activeLang('es')?>" href="<?php echo site('root').site('subdomain').'es/sostenibilidad/'?>">ESP</a>
                    <a class="lang-selector__option <?php echo activeLang('ca')?>" href="<?php echo site('root').site('subdomain').'ca/sostenibilidad/'?>">CA</a>
                </div>
            </div>
            <div class="header__close js-close-info js-info-nav is-empty"></div>
        </header>
        <div class="story-container story-container">
            <div class="story-wrapper">
                <div class="story-slide chapter" data-hash="agua-dulce" data-chapter="0">
                    <!-- Slide content chapter.php -->
                    <div class="chapter__content">
                        <img src="<?php echo site('root').site('subdomain').'img/icons/logo-blue.svg'?>" class="logo-blue" alt="Logotipo Font Vella">
                        <h1 class="title chapter__title chapter__title-separator-top">
                            <?php echo t('SLIDE_1_CHAPTER_title');?>
                        </h1>
                        <div class="chapter__separator-icon chapter__separator-icon--water"><img src="<?php echo site('root').site('subdomain').'img/icons/ico-slide-01.svg'?>"/></div>
                    </div>
                    <!-- End -->
                    <footer class="story__footer js-moreinfo js-story-nav">
                        <div class="story__more-info">
                            <a><?php echo t('BTN_saber-mas') ?></a>
                        </div>
                    </footer>
                </div>
                <div class="story-slide chapter" data-hash="espacios-naturales" data-chapter="1">
                    <div class="chapter__content">
                        <h1 class="title chapter__title chapter__title-separator-top">
                            <?php echo t('SLIDE_2_CHAPTER_title') ?>
                        </h1>
                        <div class="chapter__separator-icon chapter__separator-icon--origin"><img src="<?php echo site('root').site('subdomain').'img/icons/ico-slide-02.svg'?>"/></div>
                        <div class="chapter__logos">
                            <img src="<?php echo site('root').site('subdomain').'img/icons/logo-ajuntament-santhilari.svg'?>" alt="Logotipo Ayuntamiento Sant Hilari">
                            <img src="<?php echo site('root').site('subdomain').'img/icons/logo-siguenza.svg'?>" alt="Logotipo Sigüenza">
                            <img src="<?php echo site('root').site('subdomain').'img/icons/logo-selvans.svg'?>" alt="Logotipo Selvans">
                            <img src="<?php echo site('root').site('subdomain').'img/icons/logo-micorriza.svg'?>" alt="Logotipo Micorriza">
                        </div>
                    </div>
                    <footer class="story__footer js-moreinfo js-story-nav">
                        <div class="story__more-info">
                            <a><?php echo t('BTN_saber-mas') ?></a>
                        </div>
                    </footer>
                </div>
                <div class="story-slide chapter" data-hash="rios" data-chapter="2">
                    <div class="chapter__content">
                        <h1 class="title chapter__title chapter__title-separator-top">
                            <?php echo t('SLIDE_3_CHAPTER_title') ?>
                        </h1>
                        <div class="chapter__separator-icon chapter__separator-icon--act"><img src="<?php echo site('root').site('subdomain').'img/icons/ico-slide-04.svg'?>"/></div>
                        <div class="chapter__logos">
                            <img src="<?php echo site('root').site('subdomain').'img/icons/logo-anse.svg'?>" alt="Logotipo ANSE">
                            <img src="<?php echo site('root').site('subdomain').'img/icons/logo-red-cambera.svg'?>" alt="Logotipo Red Cambera">
                            <img class="logo-bio" src="<?php echo site('root').site('subdomain').'img/icons/logo-fundacion-bio.png'?>" alt="Logotipo Fundación Bio">
                        </div>
                    </div>
                    <footer class="story__footer js-moreinfo js-story-nav">
                        <div class="story__more-info">
                            <a><?php echo t('BTN_saber-mas') ?></a>
                        </div>
                    </footer>
                </div>
                <div class="story-slide chapter" data-hash="21-marzo" data-chapter="3">
                    <div class="chapter__content">
                        <img src="<?php echo site('root').site('subdomain').'img/icons/logo-actuamos.png'?>" class="logo-actuamos" alt="Logotipo Font Vella Actuamos por los Espacios Naturales">
                        <h1 class="title chapter__title">
                            <?php echo t('SLIDE_4_CHAPTER_title') ?>
                        </h1>
                        <div class="chapter__separator-icon chapter__separator-icon--support"><img src="<?php echo site('root').site('subdomain').'img/icons/ico-slide-03.svg'?>"/></div>
                    </div>
                    <footer class="story__footer js-moreinfo js-story-nav">
                        <div class="story__more-info">
                            <a><?php echo t('BTN_saber-mas') ?></a>
                        </div>
                    </footer>
                </div>
                <div class="story-slide chapter" data-hash="pura-naturaleza" data-chapter="4">
                    <div class="chapter__content">
                        <h1 class="title chapter__title chapter__title-separator-top">
                            <?php echo t('SLIDE_5_CHAPTER_title') ?>
                        </h1>
                        <div class="chapter__separator-icon chapter__separator-icon--protect"><img src="<?php echo site('root').site('subdomain').'img/icons/ico-slide-05.svg'?>"/></div>
                    </div>
                </div>
            </div>
            <div class="story-pagination">
                <div class="story-pagination__chapter is-completed" data-chapter="0"></div>
                <div class="story-pagination__chapter" data-chapter="1"></div>
                <div class="story-pagination__chapter" data-chapter="2"></div>
                <div class="story-pagination__chapter" data-chapter="3"></div>
                <div class="story-pagination__chapter" data-chapter="4"></div>
            </div>
            <div class="story-button story-button--prev story-button-prev"></div>
            <div class="story-button story-button--next story-button-next"></div>
        </div> <!-- End story -->
        <div class="info is-empty">
            <div class="info__outer"></div>
            <footer class="info__footer">
                <p>© Aguas Danone S.A.</p>
                <nav class="footer-nav">
                    <a href="<?php echo site('root').site('subdomain').'aviso-legal'?>"><?php echo t('FOOTER_NAV_aviso-legal') ?></a>
                    <a href="#" class="js-cookies-popup"><?php echo t('FOOTER_NAV_politica-de-cookies') ?></a>
                </nav>
                <p>Web Design: <a href="http://digitaldosis.com" target="_blank">Digital Dosis</a></p>
            </footer>
        </div>
    </div> <!-- End page-wrapper -->
<!-- <script src="js/picturefill.min.js" charset="utf-8"></script> -->
<script src="<?php echo site('root').site('subdomain').'js/main.js'?>" charset="utf-8"></script>
</body>
</html>