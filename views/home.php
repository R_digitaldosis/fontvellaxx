<?php
session_start();
require_once('translate.php');

// Output your language switcheroo-gadget
if (getLanguage() === 'es') {
    echo '<a href="' . '?lang=ca">' . t('nombreIdiomaCatalan') . '</a>';
} else {
    echo '<a href="' . '?lang=es">' . t('nombreIdiomaEspañol') . '</a>';
}

echo '<h1>' . 'H O M E' . '</h1>';
?>