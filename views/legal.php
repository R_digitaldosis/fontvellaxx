<!DOCTYPE html>
<html lang="<?php echo getLanguage();?>">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Aviso legal | Font Vella</title>
  <link rel="stylesheet" href="<?php echo site('root').site('subdomain').'css/main.min.css'?>">
  <link rel="stylesheet" href="<?php echo site('root').site('subdomain').'css/vendors.min.css'?>">
  <script type="text/javascript" src="https://connectdanoneesprew4.housings.nexica.net/WATS-FONTV201804/privacy_es.js" ></script>
  <script src="<?php echo site('root').site('subdomain').'js/vendors.min.js'?>" charset="utf-8"></script>
  <!-- Google Tag Manager -->
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
    new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
    j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
    'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-TBXMQF2');</script>
    <!-- End Google Tag Manager -->
</head>
<body>
    <!-- Google Tag Manager (noscript) -->
    <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-TBXMQF2"
    height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
    <!-- End Google Tag Manager (noscript) -->
    <div class="page legal">
        <div class="loader js-loader">
          <div class="loader__box js-loader-box">
            <div class="loader__background"></div>
            <div class="loader__progress js-progress"></div>
            <span class="loader__logo">
              <img src="<?php echo site('root').site('subdomain').'img/icons/logo-preloader.png'?>"/>
            </span>
          </div>
        </div>
        <header class="header header--story">
            <div class="header__logo header__logo--grey"><a href="/"><h1>Font Vella</h1></a></div>
            <a href="/"><div class="header__close header__close--grey js-close-legal js-info-nav"></div></a>
        </header>
        <div class="legal-container">
            <div class="legal__content">
                <h1><?php echo 'Aviso Legal'?></h1>
                <h5>1. Identificaci&oacute;n del titular de la Web</h5>
                <p>Raz&oacute;n social: Aguas Danone, S.A.</p>
                <p>Domicilio Social: Calle Buenos Aires 21, 08029 Barcelona</p>
                <p>Datos Registrales: Tomo 33825, Folio 40, Hoja B-57106 del Registro Mercantil de Barcelona.</p>
                <p>CIF: A-08016255</p>
                <p>Tlf.: (+34) 932 272 500</p>
                <h5>2. Aceptaci&oacute;n del Usuario</h5>
                <p>Las presentes Condiciones de Uso regulan el acceso y la utilizaci&oacute;n de la p&aacute;gina Web <a href="http://www.fontvella.es/">www.fontvella.es</a> (en adelante, la Web) que Aguas Danone, S.A. (en adelante, FONT VELLA) pone a disposici&oacute;n de los Usuarios en Internet.</p>
                <p>FONT VELLA puede ofrecer a trav&eacute;s de la p&aacute;gina Web, servicios que podr&aacute;n encontrarse sometidos a unas condiciones particulares propias sobre las cuales se informar&aacute; al Usuario en cada caso concreto.</p>
                <p>El acceso y la navegaci&oacute;n en la misma implica la aceptaci&oacute;n sin reservas de las presentes Condiciones de uso.</p>
                <h5>3. Acceso a la p&aacute;gina Web</h5>
                <p>Esta p&aacute;gina Web va dirigida exclusivamente a Usuarios mayores de edad (18 a&ntilde;os). En general no se exige el previo registro como Usuario para el acceso y uso de la p&aacute;gina Web, sin perjuicio de que para la utilizaci&oacute;n de determinados servicios o contenidos de la misma se deba realizar dicho registro.</p>
                <p>FONT VELLA adopta las medidas t&eacute;cnicas y organizativas necesarias para garantizar la protecci&oacute;n de los datos de car&aacute;cter personal y evitar su alteraci&oacute;n, p&eacute;rdida, tratamiento y/o acceso no autorizado, habida cuenta del estado de la t&eacute;cnica, la naturaleza de los datos almacenados y los riesgos a que est&aacute;n expuestos, todo ello, conforme a lo establecido por la legislaci&oacute;n espa&ntilde;ola de Protecci&oacute;n de Datos de Car&aacute;cter Personal.</p>
                <p>FONT VELLA no se hace responsable frente a los Usuarios, por la revelaci&oacute;n de sus datos personales a terceros que no sea debida a causas directamente imputables a FONT VELLA, ni por el uso que de tales datos hagan terceros ajenos a FONT VELLA.</p>
                <h5>4. Uso correcto de la Web</h5>
                <p>El Usuario se compromete a utilizar la Web, los contenidos y servicios de conformidad con la Ley, las presentes Condiciones de uso, las buenas costumbres y el orden p&uacute;blico. Del mismo modo el Usuario se obliga a no utilizar la Web o los servicios que se presten a trav&eacute;s de &eacute;sta, con fines o efectos il&iacute;citos o contrarios al contenido de las presentes Condiciones de uso, lesivos de los intereses o derechos de terceros, o que de cualquier forma puedan da&ntilde;ar, inutilizar o deteriorar la Web o sus servicios, o impedir un normal disfrute de la Web por otros Usuarios.</p>
                <p>Asimismo, el Usuario se compromete expresamente a no destruir, alterar, inutilizar o, de cualquier otra forma, da&ntilde;ar los datos, programas o documentos electr&oacute;nicos que se encuentren en la Web.</p>
                <p>El Usuario se compromete a no obstaculizar el acceso de otros Usuarios al servicio de acceso mediante el consumo masivo de los recursos inform&aacute;ticos a trav&eacute;s de los cuales FONT VELLA presta el servicio, as&iacute; como realizar acciones que da&ntilde;en, interrumpan o generen errores en dichos sistemas.</p>
                <p>El Usuario se compromete a no introducir programas, virus, macros, applets, controles ActiveX o cualquier otro dispositivo l&oacute;gico o secuencia de caracteres que causen o sean susceptibles de causar cualquier tipo de alteraci&oacute;n en los sistemas inform&aacute;ticos de FONT VELLA o de terceros.</p>
                <h5>5. Publicidad</h5>
                <p>Parte del sitio Web puede albergar contenidos publicitarios o estar patrocinado. Los anunciantes y patrocinadores son los &uacute;nicos responsables de asegurarse de que el material remitido para su inclusi&oacute;n en el sitio Web cumple las leyes que en cada caso puedan ser de aplicaci&oacute;n.</p>
                <p>FONT VELLA no ser&aacute; responsable de cualquier error, inexactitud o irregularidad que puedan incluir los contenidos publicitarios o de los patrocinadores. En todo caso, para interponer cualquier reclamaci&oacute;n relacionada con los contenidos publicitarios insertados en este sitio Web, pueden dirigirse a la siguiente direcci&oacute;n de correo electr&oacute;nico: <a href="mailto:aguasdanone@external.danone.com">aguasdanone@external.danone.com</a></p>
                <h5>6. Enlaces de terceros</h5>
                <p>Las presentes Condiciones de uso se refieren &uacute;nicamente a la p&aacute;gina Web y contenidos de FONT VELLA. No se aplican a los enlaces o a las p&aacute;ginas Web de terceros accesibles a trav&eacute;s de la p&aacute;gina Web.</p>
                <p>Los destinos de dichos enlaces no est&aacute;n bajo el control de FONT VELLA, y FONT VELLA no es responsable del contenido de ninguna de las p&aacute;ginas Web de destino de un enlace, ni de ning&uacute;n enlace incluido en una p&aacute;gina Web a la que se llegue desde la Web de FONT VELLA, ni de ning&uacute;n cambio o actualizaci&oacute;n de dichas p&aacute;ginas.</p>
                <p>Estos enlaces se proporcionan &uacute;nicamente para informar al Usuario sobre la existencia de otras fuentes de informaci&oacute;n sobre un tema concreto, y la inclusi&oacute;n de un enlace no implica la aprobaci&oacute;n de la p&aacute;gina Web enlazada por parte de FONT VELLA.</p>
                <h5>7. Propiedad Intelectual e industrial</h5>
                <p>Todos los contenidos de la p&aacute;gina Web, salvo que se indique lo contrario, son titularidad exclusiva de FONT VELLA y, con car&aacute;cter enunciativo, que no limitativo, el dise&ntilde;o gr&aacute;fico, c&oacute;digo fuente, logos, textos, gr&aacute;ficos, ilustraciones, fotograf&iacute;as, y dem&aacute;s elementos que aparecen en la p&aacute;gina Web.</p>
                <p>Igualmente, todos los nombres comerciales, marcas o signos distintivos de cualquier clase contenidos en la p&aacute;gina Web est&aacute;n protegidos por la Ley.</p>
                <p>FONT VELLA no concede ning&uacute;n tipo de licencia o autorizaci&oacute;n de uso personal al Usuario sobre sus derechos de propiedad intelectual e industrial o sobre cualquier otro derecho relacionado con su Web y los servicios ofrecidos en la misma.</p>
                <p>Por ello, el Usuario reconoce que la reproducci&oacute;n, distribuci&oacute;n, comercializaci&oacute;n, transformaci&oacute;n, y en general, cualquier otra forma de explotaci&oacute;n, por cualquier procedimiento, de todo o parte de los contenidos de esta p&aacute;gina Web constituye una infracci&oacute;n de los derechos de propiedad intelectual y/o industrial de FONT VELLA o del titular de los mismos.</p>
                <h5>8. Redes sociales</h5>
                <p>El Usuario podr&aacute; unirse a las comunidades que FONT VELLA tiene en distintas redes sociales. El Usuario que se haga fan de alguno de estas comunidades, acepta las condiciones de uso y pol&iacute;tica de privacidad de la red social correspondiente.</p>
                <h5>9. Protecci&oacute;n de datos de car&aacute;cter personal</h5>
                <p>El Usuario podr&aacute; remitir a FONT VELLA sus datos de car&aacute;cter personal a trav&eacute;s de los distintos formularios o direcciones de correo electr&oacute;nico que a tal efecto aparecen incorporados en la p&aacute;gina Web. Dichos formularios o direcciones de correo electr&oacute;nico incorporan un aviso legal en materia de protecci&oacute;n de datos personales que da cumplimiento a las exigencias establecidas en la Ley Org&aacute;nica 15/1999, de 13 de diciembre, de Protecci&oacute;n de Datos de Car&aacute;cter Personal, y en su Reglamento de Desarrollo, aprobado por el Real Decreto 1720/2007, de 21 de diciembre.</p>
                <p>Rogamos lea atentamente los textos legales antes de facilitar sus datos de car&aacute;cter personal.</p>
                <p>En cualquier caso, le informamos de que puede ejercitar los derechos de acceso, rectificaci&oacute;n, cancelaci&oacute;n y oposici&oacute;n, as&iacute; como revocar el consentimiento al env&iacute;o de comunicaciones electr&oacute;nicas, en caso de que nos lo haya otorgado, mediante comunicaci&oacute;n escrita adjuntando fotocopia del DNI, dirigida a AGUAS FONT VELLA Y LANJARÓN, S.A., sita en la calle Buenos Aires, 21, 08029 Barcelona o enviando un email a <a href="mailto:aguasdanone@external.danone.com">aguasdanone@external.danone.com</a> a&ntilde;adiendo la palabra &ldquo;LOPD&rdquo; en el asunto del correo.</p>
                <h5>11. Cookies</h5>
                <p>Esta Web utiliza Cookies. Las Cookies son peque&ntilde;os ficheros de datos que se generan en el ordenador del Usuario y que permiten conocer cierta informaci&oacute;n como, por ejemplo, la fecha y la hora de la &uacute;ltima vez en que el Usuario visit&oacute; nuestra p&aacute;gina Web, el dise&ntilde;o de contenidos que escogi&oacute;, o los elementos de seguridad que intervienen en el control de acceso a las &aacute;reas restringidas.</p>
                <p>El Usuario tiene la opci&oacute;n de impedir la generaci&oacute;n de Cookies, mediante la selecci&oacute;n de la correspondiente opci&oacute;n en su programa Navegador.</p>
                <p>FONT VELLA pone a disposici&oacute;n del Usuario su Pol&iacute;tica de Cookies en la que se describe la informaci&oacute;n captada por las Cookies y las finalidades para las que &eacute;stas se utilizan, as&iacute; como los medios habilitados para desactivarlas.</p>
                <p>Le rogamos lea detenidamente la Pol&iacute;tica de Cookies.</p>
                <h5>12. Ley aplicable y jurisdicci&oacute;n</h5>
                <p>En el supuesto de que surja cualquier conflicto o discrepancia en la interpretaci&oacute;n o aplicaci&oacute;n de las presentes Condiciones de uso, los Juzgados y Tribunales que, en su caso, conocer&aacute;n del asunto ser&aacute;n los que disponga la normativa aplicable en materia de jurisdicci&oacute;n competente, y que trat&aacute;ndose de consumidores finales, se corresponder&aacute; con la del lugar del cumplimiento de la obligaci&oacute;n, o la del domicilio del consumidor.</p>
                <p>Todo ello sin perjuicio de la facultad del Usuario de acudir a la Junta Arbitral de Consumo de su demarcaci&oacute;n.</p>
                <p>En el caso de que se trate de una empresa, ambas partes se someten, con renuncia expresa a cualquier otro fuero, a los Juzgados y Tribunales de Barcelona (Espa&ntilde;a).&rdquo;</p>
            </div>
            <footer class="legal__footer">
                <p>© Aguas Danone S.A.</p>
                <nav class="footer-nav">
                    <a href="<?php echo site('root').site('subdomain').'aviso-legal'?>"><?php echo t('FOOTER_NAV_aviso-legal') ?></a>
                    <a href="#" class="js-cookies-popup"><?php echo t('FOOTER_NAV_politica-de-cookies') ?></a>
                </nav>
                <p>Web Design: <a href="http://digitaldosis.com" target="_blank">Digital Dosis</a></p>
            </footer>
        </div> <!-- End story -->
    </div> <!-- End page-wrapper -->
<!-- <script src="js/picturefill.min.js" charset="utf-8"></script> -->
<script src="<?php echo site('root').site('subdomain').'js/main.js'?>" charset="utf-8"></script>
</body>
</html>