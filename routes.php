<?php
// First, let's define our list of routes.
// We could put this in a different file and include it in order to separate
// logic and configuration.
$routes = array(
    ''       => 'story.php',
    '/'      => 'story.php',//'Welcome! This is the main page.',   
    '/sostenibilidad' => 'story.php',
    '/es/sostenibilidad' => 'story.php',
    '/ca/sostenibilidad' => 'story.php',
    '/sostenibilidad/' => 'story.php',
    '/es/sostenibilidad/' => 'story.php',
    '/ca/sostenibilidad/' => 'story.php',
    '/es'     => 'story.php',
    '/es/'    => 'story.php',
    '/ca'     => 'story.php',
    '/ca/'    => 'story.php',
    '/aviso-legal'    => 'legal.php',
    '/ca/aviso-legal'    => 'legal.php',
    '/sostenibilidad/agua-dulce' => 'story.php',
    '/sostenibilidad/espacios-naturales' => 'story.php',
    '/sostenibilidad/rios' => 'story.php',
    '/sostenibilidad/21-marzo' => 'story.php',
    '/sostenibilidad/pura-naturaleza' => 'story.php',
    '/es/sostenibilidad/agua-dulce' => 'story.php',
    '/es/sostenibilidad/espacios-naturales' => 'story.php',
    '/es/sostenibilidad/rios' => 'story.php',
    '/es/sostenibilidad/21-marzo' => 'story.php',
    '/es/sostenibilidad/pura-naturaleza' => 'story.php',
    '/ca/sostenibilidad/agua-dulce' => 'story.php',
    '/ca/sostenibilidad/espacios-naturales' => 'story.php',
    '/ca/sostenibilidad/rios' => 'story.php',
    '/ca/sostenibilidad/21-marzo' => 'story.php',
    '/ca/sostenibilidad/pura-naturaleza' => 'story.php',
    '/agua-dulce' => 'story.php',
    '/espacios-naturales' => 'story.php',
    '/rios' => 'story.php',
    '/21-marzo' => 'story.php',
    '/pura-naturaleza' => 'story.php',
    '/ca/agua-dulce' => 'story.php',
    '/ca/espacios-naturales' => 'story.php',
    '/ca/rios' => 'story.php',
    '/ca/21-marzo' => 'story.php',
    '/ca/pura-naturaleza' => 'story.php',
);

// This is our router.
function router($routes){
    // Iterate through a given list of routes.
    foreach ($routes as $path => $view) {
        if ($path == site('uri')) { //if ('/fontvellaxx' . $path == site('uri')) {
            // If the path matches, display its contents and stop the router.
            require  site('views_path') . '/' . $view;
            return;
        }
    }

    // This can only be reached if none of the routes matched the path.
    //echo 'Sorry! Page not found '. $_SERVER['REQUEST_URI'];
    //http_response_code(404);
    require site('views_path') . '/story.php';
    //echo site('views_path') . '/404.php';
}

// Execute the router with our list of routes.
router($routes);
?>