<?php
// Store site information
// We could put this in a different file and include it in order to separate
// logic and configuration.

/**
 * Used to get website information.
 *
 * @var string
 */
function site($key = '') {
	$root = (isset($_SERVER['HTTPS']) ? 'https' : 'http') . "://$_SERVER[HTTP_HOST]";

	$site = [
	    'root'  => $root,
	    'subdomain'  => '/fontvellaxx/',
	    'url'   => $root . $_SERVER['REQUEST_URI'],
	    'uri'   => $_SERVER['REQUEST_URI'],
	    'views_path' => 'views',
	    'content_path' => 'content',
	    'data_path' => 'data'
	];

    return isset($site[$key]) ? $site[$key] : null;
}
?>